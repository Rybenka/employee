package employees;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Tabelka extends JFrame {

    private JTable table;
    private final static String FILE_NAME = "baza.dat";
    private int rowSelected;
    private static final long serialVersionUID = -7962404280558108365L;

    public int getRowSelected() {
        return rowSelected;
    }

    public void setRowSelected(int rowSelected) {
        this.rowSelected = rowSelected;
    }

    public Tabelka(JTable p_table) {
        table = p_table;
        this.setSize(new Dimension(600, 300));
        this.setTitle("Dane pracownikow");
        this.setLayout(new BorderLayout());
        this.add(table.getTableHeader(), BorderLayout.PAGE_START);
        this.add(table, BorderLayout.CENTER);
        if (table.getColumnCount() > 3) {
            JPanel buttonPanel = new JPanel();
            JButton save = new JButton("Zapisz zmiany");
            save.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    File file = new File(FILE_NAME);
                    try (FileWriter fw = new FileWriter(file.getAbsoluteFile()); BufferedWriter bw = new BufferedWriter(fw);) {
                        if (!file.exists()) {
                            boolean created = file.createNewFile();
                            System.out.println(created);
                        }
                        for (int i = 0; i < p_table.getRowCount(); i++) {
                            bw.write(p_table.getModel().getValueAt(i, 0) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 1) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 2) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 3) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 4) + " - ");// salary
                            bw.write(p_table.getModel().getValueAt(i, 5) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 6) + " - ");
                            bw.write(p_table.getModel().getValueAt(i, 7) + "\n");
                        }
                        dispose();
                        JOptionPane.showMessageDialog(null, "Zapisano dane");
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                }
            });
            buttonPanel.add(save);
            JButton cancel = new JButton("Anuluj");
            cancel.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    dispose();
                }
            });
            buttonPanel.add(cancel);
            this.add(buttonPanel, BorderLayout.SOUTH);
        }
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public Tabelka(Employee em, JTable p_table, MyDefaultTableModel p_dtm) {
        this.setSize(new Dimension(600, 300));
        this.setTitle("Edytuj pracownika");
        this.setLayout(new BorderLayout());
        JPanel north = new JPanel();
        JLabel imie = new JLabel("Imie: " + em.getImie());
        JLabel nazwisko = new JLabel("Nazwisko: ");
        JTextField nazwiskoPracownikaEdit = new JTextField(em.getNazwisko());
        JLabel nazwiskoPracownikaNonEdit = new JLabel(em.getNazwisko());
        north.add(imie);
        north.add(nazwisko);
        if (em.getPlec() == 'M') {
            north.add(nazwiskoPracownikaNonEdit);
        } else {
            north.add(nazwiskoPracownikaEdit);
        }
        this.add(north, BorderLayout.NORTH);
        JPanel center = new JPanel();
        center.setLayout(new FlowLayout());
        JLabel plec = new JLabel("Plec: " + em.getPlec());
        JLabel nrDzialu = new JLabel("Nr Dzialu: ");
        JTextField numerDzialu = new JTextField();
        numerDzialu.setText(em.getNr_dzialu() + "");
        numerDzialu.setPreferredSize(new Dimension(20, 20));
        JLabel placa = new JLabel("Placa: ");
        JTextField pl = new JTextField();
        pl.setText(em.getPlaca() + "");
        center.add(plec);
        center.add(nrDzialu);
        center.add(numerDzialu);
        center.add(placa);
        center.add(pl);
        JLabel wiek = new JLabel("Wiek: ");
        JTextField wk = new JTextField();
        wk.setText(em.getWiek() + "");
        JLabel dzieci = new JLabel("Dzieci: ");
        JTextField dt = new JTextField(em.getDzieci());
        dt.setText(em.getDzieci() + "");
        JLabel stanCywilny = new JLabel("Stan Cywilny: ");
        center.add(wiek);
        center.add(wk);
        center.add(dzieci);
        center.add(dt);
        center.add(stanCywilny);
        JComboBox<String> sc = new JComboBox<String>();
        sc.addItem("Wolny(a)");
        sc.addItem("Zonaty/Mezatka");
        if (em.isStan_cywilny() == true) {
            sc.setSelectedItem("Zonaty/Mezatka");
        } else {
            sc.setSelectedItem("Wolny(a)");
        }
        center.add(sc);
        this.add(center, BorderLayout.CENTER);
        JPanel south = new JPanel();
        JButton zapisz = new JButton("Zapisz");
        zapisz.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (em.getPlec() == 'K' && nazwiskoPracownikaEdit.getText().equals("")) {
                        JOptionPane.showMessageDialog(null, "Prosze wpisac nazwisko pracownika!");
                    }
                    if (numerDzialu.getText().equals("") || pl.getText().equals("") || wk.getText().equals("") || dt.getText().equals("")) {
                        JOptionPane.showMessageDialog(null, "Prosze uzupelnic wszystkie pola!");

                    } else {
                        Integer.parseInt(numerDzialu.getText());
                        Float.parseFloat(pl.getText());
                        Integer.parseInt(wk.getText());
                        Integer.parseInt(dt.getText());
                        p_table.setValueAt(nazwiskoPracownikaEdit.getText(), p_table.getSelectedRow(), 1);
                        p_table.setValueAt(numerDzialu.getText(), p_table.getSelectedRow(), 3);
                        p_table.setValueAt(pl.getText(), p_table.getSelectedRow(), 4);
                        p_table.setValueAt(wk.getText(), p_table.getSelectedRow(), 5);
                        p_table.setValueAt(dt.getText(), p_table.getSelectedRow(), 6);
                        p_table.setValueAt(sc.getSelectedItem(), p_table.getSelectedRow(), 7);
                    }
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Prosze wpisac wartosc liczbowa!");
                }
                dispose();
            }
        });
        south.add(zapisz);
        JButton usun = new JButton("Usun pracownika");
        usun.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setRowSelected(p_table.getSelectedRow());
                p_dtm.removeRow(rowSelected);
                dispose();
            }
        });
        south.add(usun);
        JButton export = new JButton("Eksportuj dane");
        export.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                fc.setFileFilter(new FileNameExtensionFilter("txt file", ".txt"));
                int returnVal = fc.showSaveDialog(Tabelka.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    String fname = file.getAbsolutePath();
                    try (FileWriter fw = new FileWriter(file.getAbsoluteFile(), false); BufferedWriter bw = new BufferedWriter(fw);) {
                        if (!fname.endsWith(".txt")) {
                            file = new File(fname + ".txt");
                        }
                        boolean created = file.createNewFile();
                        System.out.println(created);
                        bw.write(em.getNazwisko() + " " + em.getImie() + " " + em.getPlec() + " " + em.getNr_dzialu() + " " + em.getPlaca() + " "
                                + em.getWiek() + " " + em.getDzieci());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                JOptionPane.showMessageDialog(null, "Wyeksportowano dane");
            }
        });
        south.add(export);
        this.add(south, BorderLayout.SOUTH);
        pack();
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public Tabelka() {
        this.setSize(new Dimension(600, 300));
        this.setTitle("Dodaj nowego pracownika");
        this.setLayout(new BorderLayout());
        JPanel north = new JPanel();
        JLabel imie = new JLabel("Imie: ");
        JTextField imiePracownika = new JTextField();
        imiePracownika.setPreferredSize(new Dimension(100, 20));
        JLabel nazwisko = new JLabel("Nazwisko: ");
        JTextField nazwiskoPracownika = new JTextField();
        nazwiskoPracownika.setPreferredSize(new Dimension(150, 20));
        north.add(imie);
        north.add(imiePracownika);
        north.add(nazwisko);
        north.add(nazwiskoPracownika);
        this.add(north, BorderLayout.NORTH);
        JPanel center = new JPanel();
        center.setLayout(new FlowLayout());
        JLabel plec = new JLabel("Plec: ");
        JComboBox<String> ple = new JComboBox<String>();
        ple.addItem("K - Kobieta");
        ple.addItem("M - Mezczyzna");
        JLabel nrDzialu = new JLabel("Nr Dzialu: ");
        JTextField numerDzialu = new JTextField();
        numerDzialu.setPreferredSize(new Dimension(40, 20));
        JLabel placa = new JLabel("Placa: ");
        JTextField pl = new JTextField();
        pl.setPreferredSize(new Dimension(40, 20));
        center.add(plec);
        center.add(ple);
        center.add(nrDzialu);
        center.add(numerDzialu);
        center.add(placa);
        center.add(pl);
        JLabel wiek = new JLabel("Wiek: ");
        JTextField wk = new JTextField();
        wk.setPreferredSize(new Dimension(20, 20));
        JLabel dzieci = new JLabel("Dzieci: ");
        JTextField dt = new JTextField();
        dt.setPreferredSize(new Dimension(20, 20));
        JLabel stanCywilny = new JLabel("Stan Cywilny: ");
        center.add(wiek);
        center.add(wk);
        center.add(dzieci);
        center.add(dt);
        center.add(stanCywilny);
        JComboBox<String> sc = new JComboBox<String>();
        sc.addItem("Wolny(a)");
        sc.addItem("Zonaty/Mezatka");
        center.add(sc);
        this.add(center, BorderLayout.CENTER);
        JPanel south = new JPanel();
        JButton zapisz = new JButton("Zapisz");

        zapisz.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                File file = new File(FILE_NAME);
                try (FileWriter fw = new FileWriter(file.getAbsoluteFile(), true); BufferedWriter bw = new BufferedWriter(fw);) {
                    if (!file.exists()) {
                        boolean created = file.createNewFile();
                        System.out.println(created);;
                    }
                    String plecFinal = "";
                    if (ple.getSelectedItem().equals("K - Kobieta")) {
                        plecFinal = "K";
                    } else {
                        plecFinal = "M";
                    }
                    try {
                        if (imiePracownika.getText().equals("") || nazwiskoPracownika.getText().equals("") || numerDzialu.getText().equals("")
                                || pl.getText().equals("") || wk.getText().equals("") || dt.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Prosze uzupelnic wszystkie pola!");
                        } else {
                            Integer.parseInt(numerDzialu.getText());
                            Float.parseFloat(pl.getText());
                            Integer.parseInt(wk.getText());
                            Integer.parseInt(dt.getText());
                            bw.write("\n" + imiePracownika.getText() + " - " + nazwiskoPracownika.getText() + " - " + plecFinal + " - "
                                    + numerDzialu.getText() + " - " + pl.getText() + " - " + wk.getText() + " - " + dt.getText() + " - "
                                    + sc.getSelectedItem());
                            JOptionPane.showMessageDialog(null, "Zapisano pracownika");
                            dispose();
                        }
                    } catch (NumberFormatException nfe) {
                        JOptionPane.showMessageDialog(null, "Proszę wpisać wartość liczbową!");
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        south.add(zapisz);
        this.add(south, BorderLayout.SOUTH);
        pack();
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

}
