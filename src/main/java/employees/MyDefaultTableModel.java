package employees;

import javax.swing.table.DefaultTableModel;

public class MyDefaultTableModel extends DefaultTableModel {
    
    private static final long serialVersionUID = -7962404280558108365L;

    MyDefaultTableModel(String[] columnNames, int p) {
        super(columnNames, p);
    }

    public boolean isCellEditable(int row, int cols) {
        if (cols == 0) {
            return false;
        }
        // It will make the cells of Column-1 not Editable
        return false;
    }

}
