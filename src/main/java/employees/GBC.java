package employees;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GBC extends GridBagConstraints {

    public GBC(int x, int y){
        gridx = x; //place on the JFRame
        gridy = y;
    }
    public GBC(int x, int y, int w, int h){
        this(x, y);
        gridwidth = w;
        gridheight = h;
    }
    public GBC setFill(int fill){
        this.fill = fill;//does it need to fill the whole display area?
        return this;
    }
    public GBC setAnchor(int anchor){
        this.anchor = anchor;
        return this;
    }
    public GBC setInsets(int top, int left, int bottom, int right){
        this.insets = new Insets(top, left, bottom, right); //amount of space around
        return this;
    }

}
