package employees;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GUIFrame extends JFrame {

    private static final String FILE_NAME = "baza.dat";
    private MyDefaultTableModel dtm;
    private JTable table;
    private static final String[] COLUMN_FULL = {"Imie", "Nazwisko", "Plec", "Nr_dzialu", "Placa", "Wiek", "Dzieci", "Stan_cywilny"};
    private static final String[] COLUMN_LIMITED = {"Imie", "Nazwisko", "Placa"};
    private String imie;
    private String nazwisko;
    private char pl;
    private int nrDzialu;
    private float placa;
    private int wiek;
    private int dzieci;
    private boolean stanCywilny;
    private String stCywilny;
    private Employee em;
    private Tabelka tb;
    private Tabelka t0;
    private Tabelka t1;
    private Tabelka t3;
    private JTextField jtf1;
    private JLabel jl1;
    private JComboBox<Integer> jcb2;
    private JLabel jl2;
    private JLabel jl3_1;
    private JLabel jl3_2;
    private JLabel jl4;
    private JLabel persData;
    private JLabel avgAge;
    private static final long serialVersionUID = -7962404280558108365L;

    public String getStCywilny(Employee em) {
        if (em.isStan_cywilny()) {
            stCywilny = "Zonaty/Mezatka";
        } else {
            stCywilny = "Wolny(a)";
        }
        return stCywilny;
    }

    public GUIFrame() {
        init();
    }

    public void init() {
        this.setSize(new Dimension(300, 100));
        this.setTitle("Menu glowne");
        this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

        String[] menuElements = {"1 - Lista pracownikow", "2 - Dodaj nowego pracownika", "3 - Edytuj dane", "4 - Funkcje dodatkowe",
                "5 - Funkcje dodatkowe dla plikow tekstowych", "6 - Informacje o programie"};
        JComboBox<String> jcb = new JComboBox<String>(menuElements);

        JPanel jp = new JPanel();
        jp.add(jcb);

        this.add(jp);

        JButton jb = new JButton("Zatwierdź");

        jb.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                switch (jcb.getSelectedIndex()) {

                    case 0: // lista okrojona

                        try (BufferedReader br0 = new BufferedReader(new InputStreamReader(new FileInputStream(new File(FILE_NAME)),StandardCharsets.UTF_8))) {

                            String line = "";
                            dtm = new MyDefaultTableModel(COLUMN_LIMITED, 0);
                            while ((line = br0.readLine()) != null) {
                                String rawData[] = line.split(" - ");
                                String data[] = new String[3];
                                data[0] = rawData[0];
                                data[1] = rawData[1];
                                data[2] = rawData[4];
                                dtm.addRow(data);
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        table = new JTable(dtm);
                        t0 = new Tabelka(table);
                        break;
                    case 1:
                        t1 = new Tabelka();
                        break;
                    case 2:// lista pełna
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(FILE_NAME)),StandardCharsets.UTF_8))) {

                            String line = "";
                            dtm = new MyDefaultTableModel(COLUMN_FULL, 0);

                            while ((line = br.readLine()) != null) {
                                String data[] = line.split(" - ");
                                dtm.addRow(data);
                            }
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        table = new JTable(dtm);
                        t3 = new Tabelka(table);
                        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

                            @Override
                            public void valueChanged(ListSelectionEvent e) {
                                int selRow;
                                if (tb != null) {
                                    selRow = tb.getRowSelected();
                                } else {
                                    selRow = table.getSelectedRow();
                                }
                                imie = table.getValueAt(selRow, 0).toString();
                                nazwisko = table.getValueAt(selRow, 1).toString();
                                String plec = table.getValueAt(selRow, 2).toString();
                                pl = plec.charAt(0);
                                nrDzialu = Integer.parseInt(table.getValueAt(selRow, 3).toString());
                                placa = Float.parseFloat(table.getValueAt(selRow, 4).toString());
                                wiek = Integer.parseInt(table.getValueAt(selRow, 5).toString());
                                dzieci = Integer.parseInt(table.getValueAt(selRow, 6).toString());
                                stanCywilny = true;
                                String sc = table.getValueAt(selRow, 7).toString();
                                if (sc.equals("Zonaty/Mezatka")) {
                                    stanCywilny = true;
                                } else if (sc.equals("Wolny(a)")) {
                                    stanCywilny = false;
                                }
                                em = new Employee(imie, nazwisko, pl, nrDzialu, placa, wiek, dzieci, stanCywilny);
                                if (tb == null) {
                                    tb = new Tabelka(em, table, dtm);
                                }
                            }
                        });
                        tb = null;
                        break;
                    case 3:
                        JFrame jf3 = new JFrame("Funkcje dodatkowe");
                        jf3.setSize(new Dimension(600, 400));
                        jf3.setLayout(new GridBagLayout());
                        JButton b1 = new JButton("Zwroc liczbe pracownikow z pensja nie mniejsza niz:");

                        b1.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                try {
                                    float salaryProvided = Float.parseFloat(jtf1.getText());
                                    List<Employee> emList = returnEmpList();
                                    int counter = (int) emList.stream().filter(em -> em.isSalaryAbove(salaryProvided)).count();
                                    jl1.setText(String.valueOf(counter));
                                } catch (NumberFormatException nfe) {
                                    JOptionPane.showMessageDialog(null, "Prosze wpisac wartosc liczbowa!");
                                }
                            }
                        });

                        jf3.add(b1, new GBC(0, 0, 3, 1).setInsets(10, 10, 10, 10));
                        jtf1 = new JTextField(10);
                        jf3.add(jtf1, new GBC(3, 0));
                        jl1 = new JLabel("0");
                        jf3.add(jl1, new GBC(4, 0).setInsets(10, 10, 10, 10));
                        JButton b2 = new JButton("Oblicz srednia place w dziale nr:");
                        b2.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int depNumber = (int) jcb2.getSelectedItem();
                                List<Employee> emList = returnEmpList();
                                List<Employee> departmentEmployees = emList.stream().filter(em -> em.getNr_dzialu() == depNumber)
                                        .collect(Collectors.toList());
                                float average = getAverageSalary(departmentEmployees);
                                jl2.setText(String.valueOf(average));
                            }
                        });
                        jf3.add(b2, new GBC(0, 1).setInsets(10, 10, 10, 10));
                        jcb2 = new JComboBox<Integer>();
                        int[] depts = extractDepartmentNumbers();
                        for (int s : depts) {
                            jcb2.addItem(s);
                        }
                        jf3.add(jcb2, new GBC(3, 1));
                        jl2 = new JLabel("0");
                        jf3.add(jl2, new GBC(4, 1).setInsets(10, 10, 10, 10));
                        JButton b3 = new JButton("Oblicz najwyzsza place wsrod kobiet i mezczyzn:");
                        b3.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Employee> women = returnWomenList();
                                List<Employee> men = returnMenList();
                                float salaryMaxW = (float) women.stream().mapToDouble(Employee::getPlaca).max().getAsDouble();
                                float salaryMaxM = (float) men.stream().mapToDouble(Employee::getPlaca).max().getAsDouble();
                                jl3_1.setText(String.valueOf(salaryMaxW));
                                jl3_2.setText(String.valueOf(salaryMaxM));
                            }
                        });
                        jf3.add(b3, new GBC(0, 2));
                        JLabel k = new JLabel("Kobiety: ");
                        jf3.add(k, new GBC(0, 3).setAnchor(GridBagConstraints.LINE_END));
                        jl3_1 = new JLabel("0");
                        jf3.add(jl3_1, new GBC(1, 3).setInsets(0, 0, 0, 50));
                        JLabel m = new JLabel("Mezczyzni: ");
                        jf3.add(m, new GBC(2, 3).setAnchor(GridBagConstraints.LINE_END));
                        jl3_2 = new JLabel("0");
                        jf3.add(jl3_2, new GBC(3, 3).setInsets(0, 0, 0, 50));
                        JButton b4 = new JButton("Srednia placa kobiet : srednia placa mezczyzn:");
                        b4.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Employee> women = returnWomenList();
                                List<Employee> men = returnMenList();
                                float avgWomen = getAverageSalary(women);
                                float avgMen = getAverageSalary(men);

                                jl4.setText(String.valueOf(avgWomen / avgMen));
                            }
                        });
                        jf3.add(b4, new GBC(0, 4).setInsets(10, 10, 10, 10));
                        jl4 = new JLabel("0");
                        jf3.add(jl4, new GBC(3, 4).setInsets(0, 0, 0, 100));
                        JButton b5 = new JButton("Zwiększ wszystkim pensję o 10%");
                        b5.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                File file = new File(FILE_NAME);
                                List<Employee> employees = returnEmpList();
                                try (FileWriter fw = new FileWriter(file.getAbsoluteFile(), false); BufferedWriter bw = new BufferedWriter(fw);) {
                                    if (!file.exists()) {
                                        boolean created = file.createNewFile();
                                        System.out.println(created);
                                    }
                                    for (Employee emp : employees) {
                                        emp.raiseSalary(10);
                                        stCywilny = getStCywilny(emp);
                                        bw.write(emp.getImie() + " - " + emp.getNazwisko() + " - " + emp.getPlec() + " - " + emp.getNr_dzialu() + " - "
                                                + emp.getPlaca() + " - " + emp.getWiek() + " - " + emp.getDzieci() + " - " + stCywilny + "\n");
                                    }

                                    JOptionPane.showMessageDialog(null, "Zwiekszono pensje");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        jf3.add(b5, new GBC(0, 5).setInsets(10, 10, 10, 10));
                        pack();
                        jf3.setVisible(true);
                        break;
                    case 4:
                        JFrame jf4 = new JFrame("Funkcje dodatkowe dla plikow tekstowych");
                        jf4.setSize(new Dimension(600, 200));
                        jf4.setLayout(new BoxLayout(jf4.getContentPane(), BoxLayout.Y_AXIS));
                        JButton personalData = new JButton("Dane osoby z nadluzszym nazwiskiem");
                        personalData.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Employee> emList = returnEmpList();
                                emList.sort(Comparator.comparing(Employee::getNazwisko));
                                Employee longest = emList.get(0);
                                stCywilny = getStCywilny(longest);
                                persData.setText(longest.getImie() + " - " + longest.getNazwisko() + " - " + longest.getPlec() + " - "
                                        + longest.getNr_dzialu() + " - " + longest.getPlaca() + " - " + longest.getWiek() + " - " + longest.getDzieci()
                                        + " - " + stCywilny);
                            }
                        });
                        jf4.add(personalData);
                        persData = new JLabel("0");
                        jf4.add(persData);
                        JButton averageAge = new JButton("Sredni wiek osob z dziecmi");
                        averageAge.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<Employee> empWCh = returnEmployeesWithChildrenList();
                                double averageAge = empWCh.stream().mapToDouble(Employee::getWiek).average().getAsDouble();
                                avgAge.setText(String.valueOf(averageAge));
                            }
                        });
                        jf4.add(averageAge);
                        avgAge = new JLabel("0");
                        jf4.add(avgAge);
                        JButton codeSurname = new JButton("Zakoduj nazwiska osob z pensja ponizej sredniej");
                        codeSurname.addActionListener(new ActionListener() {

                            @Override
                            public void actionPerformed(ActionEvent e) {
                                List<employees.Employee> employees = returnEmpList();
                                float avgSalary = getAverageSalary(employees);
                                File file = new File(FILE_NAME);

                                try (FileWriter fw = new FileWriter(file.getAbsoluteFile(), false); BufferedWriter bw = new BufferedWriter(fw);) {
                                    if (!file.exists()) {
                                        boolean created = file.createNewFile();
                                        System.out.println(created);
                                    }
                                    for (Employee emp : employees) {
                                        emp.raiseSalary(10);
                                        stCywilny = getStCywilny(emp);
                                        if (emp.getPlaca() < avgSalary) {
                                            bw.write(emp.getImie() + " - " + codeSurname(emp.getNazwisko()) + " - " + emp.getPlec() + " - "
                                                    + emp.getNr_dzialu() + " - " + emp.getPlaca() + " - " + emp.getWiek() + " - " + emp.getDzieci()
                                                    + " - " + stCywilny + "\n");
                                        } else {
                                            bw.write(emp.getImie() + " - " + emp.getNazwisko() + " - " + emp.getPlec() + " - " + emp.getNr_dzialu()
                                                    + " - " + emp.getPlaca() + " - " + emp.getWiek() + " - " + emp.getDzieci() + " - " + stCywilny
                                                    + "\n");
                                        }
                                    }

                                    JOptionPane.showMessageDialog(null, "Nazwiska zakodowane");
                                } catch (IOException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        jf4.add(codeSurname);
                        jf4.setVisible(true);
                        break;
                    case 5:
                        File file = new File("README.txt");
                        Desktop desktop = Desktop.getDesktop();
                        try {
                            desktop.open(file);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        this.add(jb);

        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public int[] extractDepartmentNumbers() {
        List<Employee> emList = returnEmpList();
        Set<Integer> departments = emList.stream().map(e -> e.getNr_dzialu()).collect(Collectors.toSet());

        int[] depts = new int[departments.size()];
        int index = 0;
        for (Integer i : departments) {
            depts[index++] = i;
        }
        return depts;
    }

    public List<Employee> returnEmpList() {
        List<Employee> empList = new ArrayList<Employee>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(FILE_NAME)),StandardCharsets.UTF_8))) {
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] array = line.split(" - ");
                boolean stan_cywilny;
                if (array[7].equals("Wolny(a)")) {
                    stan_cywilny = false;
                } else {
                    stan_cywilny = true;
                }
                empList.add(new Employee(array[0], array[1], array[2].charAt(0), Integer.parseInt(array[3]), Float.parseFloat(array[4]),
                        Integer.parseInt(array[5]), Integer.parseInt(array[6]), stan_cywilny));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return empList;
    }

    public List<Employee> returnWomenList() {
        return returnManList('K');
    }

    public List<Employee> returnMenList() {
        return returnManList('M');
    }

    private List<Employee> returnManList(char sex) {
        return returnEmpList().stream().filter(e -> e.getPlec() == sex).collect(Collectors.toList());
    }

    public List<Employee> returnEmployeesWithChildrenList() {
        return returnEmpList().stream().filter(e -> e.getDzieci() > 0).collect(Collectors.toList());
    }

    public float getAverageSalary(List<Employee> emList) {
        return (float) emList.stream().mapToDouble(Employee::getPlaca).average().getAsDouble();
    }

    public String codeSurname(String p_surname) {
        String coded = p_surname.substring(0, 1);
        for (int i = 1; i < p_surname.length() - 1; i++) {
            coded = coded.concat("*");
        }
        coded = coded.concat(p_surname.substring(p_surname.length() - 1, p_surname.length()));
        return coded;
    }
}
