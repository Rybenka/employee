package employees;

public class Employee {

    private String imie;
    private String nazwisko;
    private char plec; // 'K' – kobieta, 'M' – mężczyzna
    private int nr_dzialu;///
    private float placa;
    private int wiek;
    private int dzieci;
    private boolean stan_cywilny; // true – mężatka / żonaty

    public Employee(String imie, String nazwisko, char plec, int nr_dzialu, float placa, int wiek, int dzieci, boolean stan_cywilny) {
        super();
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
        this.nr_dzialu = nr_dzialu;
        this.placa = placa;
        this.wiek = wiek;
        this.dzieci = dzieci;
        this.stan_cywilny = stan_cywilny;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public char getPlec() {
        return plec;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }

    public int getNr_dzialu() {
        return nr_dzialu;
    }

    public void setNr_dzialu(int nr_dzialu) {
        this.nr_dzialu = nr_dzialu;
    }

    public float getPlaca() {
        return placa;
    }

    public void setPlaca(float placa) {
        this.placa = placa;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getDzieci() {
        return dzieci;
    }

    public void setDzieci(int dzieci) {
        this.dzieci = dzieci;
    }

    public boolean isStan_cywilny() {
        return stan_cywilny;
    }

    public void setStan_cywilny(boolean stan_cywilny) {
        this.stan_cywilny = stan_cywilny;
    }

    public void getEmployeeFullData() {
        getEmployeeShortData();
        System.out.println("Płeć: " + this.plec);
        System.out.println("Nr działu: " + this.nr_dzialu);
        System.out.println("Wiek: " + this.wiek);
        System.out.println("Liczba dzieci" + this.dzieci);
        if (this.stan_cywilny) {
            System.out.println("Stan cywilny: mężatka / żonaty");
        } else {
            System.out.println("Stan cywilny: wolny");
        }
    }

    public void getEmployeeShortData() {
        System.out.println("Imie: " + this.imie);
        System.out.println("Nazwisko: " + this.nazwisko);
        System.out.println("Płaca: " + this.placa);

    }

    public void getBlockCapitalData() {
        System.out.println(this.imie.toUpperCase() + " " + this.nazwisko.toUpperCase());
    }

    public boolean isSalaryAbove(float salaryToCompare) {
        if (this.placa >= salaryToCompare) {
            return true;
        } else {
            return false;
        }
    }

    public void raiseSalary(int percentage) {
        float mnoznik = (float) percentage / 100;
        float dodatek = 0.0002f;
        if (this.dzieci > 0 && this.stan_cywilny == true) {
            this.placa = this.placa * (1 + mnoznik + 0.03f + this.dzieci * 0.02f) + dodatek;
        } else if (this.dzieci == 0 && this.stan_cywilny == true) {
            this.placa = this.placa * (1 + mnoznik + 0.03f) + dodatek;
        } else if (this.dzieci > 0 && this.stan_cywilny == false) {
            this.placa = this.placa * (1 + mnoznik + this.dzieci * 0.02f) + dodatek;
        } else {
            this.placa = this.placa * (1 + mnoznik);
        }
    }

}
